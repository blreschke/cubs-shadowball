import discord
from discord.ext import commands

intents = discord.Intents.default()
intents.message_content = True

client = commands.Bot(command_prefix="!", intents=intents)
client.remove_command("help")

scores = dict()
guesses = dict()
counter = dict()


@client.command()
async def ping(ctx):
    await ctx.send("Pong!")

@client.command()
async def guess(ctx):
    command_length = 7
    number = int(ctx.message.content[(command_length):])
    name = ctx.author.display_name
    guesses[name] = number
    addCount(name)

    await ctx.message.add_reaction('✅')

@client.command()
async def printguess(ctx):
    command_length = 12
    await ctx.send(guesses[ctx.author.id])

@client.command()
async def pitch(ctx):
    command_length = 7
    number = int(ctx.message.content[(command_length):])
    msg = "Pitch was " + str(number) + " resulting in...\n"
    any_score = False
    for user, guess in guesses.items():
        diff = abs(getDiff(guess, number))
        score = 0
        match diff: 
            case diff if diff <= 10:
                score = 5
            case diff if diff <= 25:
                score = 4
            case diff if diff <= 50:
                score = 3
            case diff if diff <= 100:
                score = 2
            case diff if diff <= 175:
                score = 1
        addScore(user, score)
        if score != 0:
            any_score = True    
            msg += user + " scoring **" + str(score) + "** with a guess of " + str(guess) + "!\n"
    if not any_score:
        msg += "No scoring :("
    guesses.clear()
    await ctx.send(msg)



@client.command()
async def scoreboard(ctx):
    await ctx.send(getScoreboard())

@client.command()
async def clear(ctx):
    scores.clear()
    counter.clear()
    guesses.clear()
    await ctx.send("Scores reset!")

@client.listen()
async def on_message(message):
    if (str(message.channel.id) is "599478399144296458"):
        print(message.channel.id)
        print(message.embeds)
        print(message.embeds[0].author)


def addScore(user, score):
    if user in scores.keys():
        scores[user] = scores[user] + score
    else:
        scores[user] = score

def addCount(user):
    if user in counter.keys():
        counter[user] = counter[user] + 1
    else:
        counter[user] = 1

def getScoreboard():
    msg = "SCORES\n"
    sorted_scores = sorted(scores.items(), key=lambda x : x[1], reverse=True)
    for pair in sorted_scores:
        name = pair[0]
        score = str(pair[1])
        attempts = str(counter[name])
        msg += name + ": " + score + " for " + attempts + "\n"
    return msg


def getDiff(num1, num2):
    """
    Calculate diff of two numbers within a range of -500 to 500
    
        Parameters:
            num1 - previous number
            num2 - current number
        Return:
            diff between numbers within range of -500 to 500
    """
    try:
        diff = int(num2) - int(num1)
        if diff <= -500 or diff > 500:
            if num2 > num1:
                return num2 - num1 - 1000
            else:
                return num2 - num1 + 1000
        else:
            return diff
    except:
        return None

client.run(open("token.txt").readlines()[0])